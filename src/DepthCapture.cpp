#include "DepthCapture.h"

#include <condition_variable>
#include <mutex>
#include <stdio.h>

#include <ST/CaptureSession.h>
#include <ST/MathTypes.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

#include <boost/filesystem.hpp>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/point_types.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/poisson.h>
#include <pcl/filters/passthrough.h>

#include <string>
#include <iostream>
#include <fstream>

using namespace pcl;
using namespace std;

struct SessionDelegate : ST::CaptureSessionDelegate {
    std::mutex lock;
    std::condition_variable cond;
    bool ready = false;
    bool done = false;
	int frame = 0;
	const string outputFile;

	SessionDelegate(string outputFile): outputFile(move(outputFile)) {
	}

    void captureSessionEventDidOccur(ST::CaptureSession *, ST::CaptureSessionEventId event) override {
        printf("Received capture session event %d (%s)\n", (int)event, ST::CaptureSessionSample::toString(event));
        switch (event) {
            case ST::CaptureSessionEventId::Ready: {
                std::unique_lock<std::mutex> u(lock);
                ready = true;
                cond.notify_all();
            } break;
            case ST::CaptureSessionEventId::Disconnected:
            case ST::CaptureSessionEventId::EndOfFile:
            case ST::CaptureSessionEventId::Error: {
                std::unique_lock<std::mutex> u(lock);
                done = true;
                cond.notify_all();
            } break;
            default:
                printf("Event %d unhandled\n", (int)event);
        }
    }

    void captureSessionDidOutputSample(ST::CaptureSession * session, const ST::CaptureSessionSample& sample) {
        if (done) {
            printf("Ignoring frame as 'done' already set to true");
            return;
        }

        //printf("Received capture session sample of type %d (%s)\n", (int)sample.type, ST::CaptureSessionSample::toString(sample.type));
        switch (sample.type) {
            case ST::CaptureSessionSample::Type::DepthFrame:
                printf("Depth frame: size %dx%d\n", sample.depthFrame.width(), sample.depthFrame.height());
                break;
            case ST::CaptureSessionSample::Type::VisibleFrame:
                printf("Visible frame: size %dx%d\n", sample.visibleFrame.width(), sample.visibleFrame.height());
                break;
            case ST::CaptureSessionSample::Type::InfraredFrame:
                printf("Infrared frame: size %dx%d\n", sample.infraredFrame.width(), sample.infraredFrame.height());
                break;
            case ST::CaptureSessionSample::Type::SynchronizedFrames:
                printf("Synchronized frames: depth %dx%d visible %dx%d infrared %dx%d\n",
					sample.depthFrame.width(), sample.depthFrame.height(),
					sample.visibleFrame.width(), sample.visibleFrame.height(),
					sample.infraredFrame.width(), sample.infraredFrame.height());
				if (frame == 0) {

					saveFrameAsPCD(session, sample, this->outputFile);

					//saveFrameAsPCD(session, sample, "data/generated/points-" + std::to_string(frame) + ".pcd");
					//saveFrameAsCSV(session, sample, "data/generated/depth-" + std::to_string(frame) + ".csv");

					std::unique_lock<std::mutex> u(lock);
                    done = true;
                    cond.notify_all();
				}

				frame++;
				break;
            case ST::CaptureSessionSample::Type::AccelerometerEvent:
                //printf("Accelerometer event: [% .5f % .5f % .5f]\n", sample.accelerometerEvent.acceleration().x, sample.accelerometerEvent.acceleration().y, sample.accelerometerEvent.acceleration().z);
                break;
            case ST::CaptureSessionSample::Type::GyroscopeEvent:
                //printf("Gyroscope event: [% .5f % .5f % .5f]\n", sample.gyroscopeEvent.rotationRate().x, sample.gyroscopeEvent.rotationRate().y, sample.gyroscopeEvent.rotationRate().z);
                break;
            default:
                printf("Sample type %d unhandled\n", (int)sample.type);
        }
    }

	void saveFrameAsCSV(ST::CaptureSession *, const ST::CaptureSessionSample& sample, const string& outputFile) {
		printf("Saving frame as PCD\n");

		boost::filesystem::path file(outputFile.c_str());
		if (boost::filesystem::create_directories(file.parent_path())) {
			std::cout << "Created directory " << "\n";
		}

		ofstream myfile;
		myfile.open(outputFile);

		for (int y = 0; y < sample.depthFrame.height(); y++) {
			for (int x = 0; x < sample.depthFrame.width(); x++) {
				float depth = sample.depthFrame(x, y);
				if (!isinf(depth) && !isnan(depth)) {
					myfile << depth;
				}
				else {
					myfile << "NaN";
				}
				if (x < sample.depthFrame.width() - 1) {
					myfile << ",";
				}
			}
			myfile << "\n";
		}
	}

    void saveFrameAsPCD(ST::CaptureSession *, const ST::CaptureSessionSample& sample, const string& outputFile) {
		printf("Saving frame as PCD\n");

        pcl::PointCloud<pcl::PointXYZ> cloud;
        cloud.width = sample.depthFrame.width();
        cloud.height = sample.depthFrame.height();
        cloud.is_dense = false;
        cloud.points.resize(cloud.width * cloud.height);

        for (int y = 0; y < sample.depthFrame.height(); y++) {
            for (int x = 0; x < sample.depthFrame.width(); x++) {
                int i = x * y;
                float depth = sample.depthFrame(x, y);
                if (!isinf(depth) && !isnan(depth)) {
                    ST::Vector3f p = sample.depthFrame.unprojectPoint(x, y);
                    cloud.points[i].x = p.x;
                    cloud.points[i].y = p.y;
                    cloud.points[i].z = p.z;
                }
                else {
                    cloud.points[i].x = std::numeric_limits<float>::quiet_NaN();
                    cloud.points[i].y = std::numeric_limits<float>::quiet_NaN();
                    cloud.points[i].z = std::numeric_limits<float>::quiet_NaN();
                }
            }
        }

		boost::filesystem::path file(outputFile.c_str());
		if (boost::filesystem::create_directories(file.parent_path())) {
			std::cout << "Created directory " << "\n";
		}

        pcl::io::savePCDFileASCII(outputFile, cloud);
        std::cout << "Saved " << cloud.points.size() << " data points to " << outputFile << std::endl;
    }


	void saveFrameAsPCL(ST::CaptureSession *, const ST::CaptureSessionSample& sample) {
		printf("Saving frame as PCL\n");

		ofstream myfile;
		myfile.open("points.pcl");
		myfile << "# Point Cloud from Structure Core Scan\n";
		myfile << "VERSION .7\n";
		myfile << "FIELDS x y z\n";
		myfile << "SIZE 4 4 4\n";
		myfile << "TYPE F F F\n";
		myfile << "COUNT 1 1 1\n";
		myfile << "WIDTH " << sample.depthFrame.width() << "\n";
		myfile << "HEIGHT " << sample.depthFrame.height() << "\n";
		myfile << "VIEWPOINT 0 0 0 1 0 0 0\n";
		myfile << "POINTS " << (sample.depthFrame.width() * sample.depthFrame.height()) << "\n";
		myfile << "DATA ascii\n";

		for (int y = 0; y < sample.depthFrame.height(); y++) {
			for (int x = 0; x < sample.depthFrame.width(); x++) {
				ST::Vector3f p = sample.depthFrame.unprojectPoint(x, y);

				float px = std::isnan(p.x) ? 0 : p.x;
				float py = std::isnan(p.y) ? 0 : p.y;
				float pz = std::isnan(p.z) ? 0 : p.z;

				char buff[200];
				snprintf(buff, sizeof(buff), "%f %f %f", px, py, pz);
				std::string buffAsStdStr = buff;

				myfile << buff;
				myfile << "\n";
			}
		}

		myfile.close();

	}


	void saveFrameAsPLY(ST::CaptureSession *, const ST::CaptureSessionSample& sample) {
		printf("Saving frame as PLY\n");

		ofstream myfile;
		myfile.open("points.ply");
		myfile << "ply\n";
		myfile << "format ascii 1.0\n";
		myfile << "element vertex " << (sample.depthFrame.width() * sample.depthFrame.height()) << "\n";
		myfile << "property float x\n";
		myfile << "property float y\n";
		myfile << "property float z\n";
		myfile << "end_header\n";

		for (int y = 0; y < sample.depthFrame.height(); y++) {
			for (int x = 0; x < sample.depthFrame.width(); x++) {
				ST::Vector3f p = sample.depthFrame.unprojectPoint(x, y);

				float px = std::isnan(p.x) ? 0 : p.x;
				float py = std::isnan(p.y) ? 0 : p.y;
				float pz = std::isnan(p.z) ? 0 : p.z;

				char buff[200];
				snprintf(buff, sizeof(buff), "%f %f %f", px, py, pz);
				std::string buffAsStdStr = buff;

				myfile << buff;
				myfile << "\n";
			}
		}

		myfile.close();

	}

    void waitUntilReady() {
        std::unique_lock<std::mutex> u(lock);
        cond.wait(u, [this]() {
            return ready;
        });
    }

    void waitUntilDone() {
        std::unique_lock<std::mutex> u(lock);
        cond.wait(u, [this]() {
            return done;
        });
    }
};

DepthCapture::DepthCapture()
{
}

void DepthCapture::captureAsPCD(const string& inputFile, const string& outputFile)
{
	printf("Capturing depth data ...\n");

    ST::CaptureSessionSettings settings;

    // If capturing from camera
    // settings.source = ST::CaptureSessionSourceId::StructureCore;
    // settings.structureCore.depthEnabled = true;
    // settings.structureCore.visibleEnabled = true;
    // settings.structureCore.infraredEnabled = true;
    // settings.structureCore.accelerometerEnabled = true;
    // settings.structureCore.gyroscopeEnabled = true;
    //settings.structureCore.depthResolution = ST::StructureCoreDepthResolution::SXGA;
    // settings.structureCore.imuUpdateRate = ST::StructureCoreIMUUpdateRate::AccelAndGyro_200Hz;
    // settings.structureCore.depthRangeMode = ST::StructureCoreDepthRangeMode::VeryShort;
    
	// If capturing from file
	settings.source = ST::CaptureSessionSourceId::OCC;
	settings.occ.path = inputFile.c_str();

    SessionDelegate delegate(outputFile);

    ST::CaptureSession session;
    session.setDelegate(&delegate);
    if (!session.startMonitoring(settings)) {
        printf("Failed to initialize capture session\n");
        return;
    }

    printf("Waiting for session to become ready...\n");
    //delegate.waitUntilReady();
    session.startStreaming();
    delegate.waitUntilDone();
    session.stopStreaming();
    return;
}
