#ifndef DepthCapture_H
#define DepthCapture_H

#include <string>
#include <stdio.h>

using namespace std;

class DepthCapture
{
public:
  DepthCapture();

  void captureAsPCD(const string& inputFile, const string& outputFile);

};

#endif