#ifndef PointCloudHelper_H
#define PointCloudHelper_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

#include <string>

using namespace std;
using namespace pcl;

class PointCloudHelper
{

public:

    PointCloudHelper();

    PointCloud<PointXYZ>::Ptr readPointCloudFile(
        const string& pcdFile
    );

    PointCloud<PointXYZ>::Ptr filterPointCloud(
		PointCloud<PointXYZ>::Ptr cloud
    );

    PointCloud<PointXYZ>::Ptr removeOutliers(
		PointCloud<PointXYZ>::Ptr cloud
    );

    PointCloud<PointXYZ>::Ptr smoothPointCloud(
		PointCloud<PointXYZ>::Ptr cloud
    );

    PointCloud<PointNormal>::Ptr estimateNormals(
	    PointCloud<PointXYZ>::Ptr cloud
    );

	PolygonMesh::Ptr greedyTrianglesReconstruction(
		PointCloud<PointNormal>::Ptr cloud
	);

    PolygonMesh::Ptr poissonReconstruction(
	    PointCloud<PointNormal>::Ptr cloud
    );

    void saveAsPLY(
        PolygonMesh::Ptr mesh,
        const string& fileName
    );

	void saveAsVTK(
		PolygonMesh::Ptr mesh,
		const string& fileName
	);

	void saveAsPCD(
		PointCloud<PointXYZ>::Ptr cloud,
		const string& fileName
	);

};

#endif