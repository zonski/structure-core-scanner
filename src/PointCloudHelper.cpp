#include "PointCloudHelper.h"

#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_io.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/poisson.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/surface/gp3.h>

#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>

using namespace std;
using namespace pcl;

PointCloudHelper::PointCloudHelper()
{
}

PointCloud<PointXYZ>::Ptr PointCloudHelper::readPointCloudFile(const string& pcdFile)
{
	cout << "Reading Point Cloud File from " << pcdFile << endl;

    PointCloud<PointXYZ>::Ptr cloud(new PointCloud<PointXYZ>);

	if (io::loadPCDFile<PointXYZ>(pcdFile.c_str(), *cloud) == -1)
	{
		PCL_ERROR("Couldn't read PCD file \n");
		return NULL;
	}

	cout << "Successfully read Point Cloud File from " << pcdFile << endl;
	return cloud;
}


PointCloud<PointXYZ>::Ptr PointCloudHelper::filterPointCloud(
		PointCloud<PointXYZ>::Ptr cloud)
{
	cout << "begin passthrough filter" << endl;

	PointCloud<PointXYZ>::Ptr filtered(new PointCloud<PointXYZ>());
	PassThrough<PointXYZ> filter;
	filter.setInputCloud(cloud);

	filter.setFilterFieldName("x");
	filter.setFilterLimits(-100, 100);
	filter.filter(*filtered);

	filter.setFilterFieldName("y");
	filter.setFilterLimits(-300, 300);
	filter.filter(*filtered);

	filter.setFilterFieldName("z");
	filter.setFilterLimits(50, 1300);
	filter.filter(*filtered);

	cout << "passthrough filter complete" << endl;
	return filtered;
}

PointCloud<PointXYZ>::Ptr PointCloudHelper::removeOutliers(
		PointCloud<PointXYZ>::Ptr cloud)
{
	cout << "begin removing outliers" << endl;

    // Create the filtering object
    PointCloud<PointXYZ>::Ptr cloud_filtered (new PointCloud<PointXYZ>);
    StatisticalOutlierRemoval<PointXYZ> sor;
    sor.setInputCloud(cloud);
    sor.setMeanK(50);
    sor.setStddevMulThresh(1.0);
    sor.filter(*cloud_filtered);

	cout << "removing of outliers complete" << endl;
	return cloud_filtered;
}


PointCloud<PointXYZ>::Ptr PointCloudHelper::smoothPointCloud(
	PointCloud<PointXYZ>::Ptr cloud)
{
	cout << "begin smoothing point cloud" << endl;

    MovingLeastSquares<PointXYZ, PointXYZ> mls;
    mls.setInputCloud(cloud);
    mls.setSearchRadius(5.025);
    mls.setPolynomialFit(true);
    mls.setPolynomialOrder(2);
    mls.setUpsamplingMethod(MovingLeastSquares<PointXYZ, PointXYZ>::SAMPLE_LOCAL_PLANE);
	mls.setUpsamplingRadius(0.005);
	mls.setUpsamplingStepSize(0.003);

    PointCloud<PointXYZ>::Ptr cloud_smoothed (new PointCloud<PointXYZ>());
    mls.process(*cloud_smoothed);

	cout << "smoothing point cloud complete" << endl;
	return cloud_smoothed;
}

PointCloud<PointNormal>::Ptr PointCloudHelper::estimateNormals(
	PointCloud<PointXYZ>::Ptr cloud)
{
	cout << "begin normal estimation" << endl;
    NormalEstimationOMP<PointXYZ, Normal> ne;
    ne.setNumberOfThreads(1);
    ne.setInputCloud(cloud);
    ne.setRadiusSearch(2.01);
    
	//Eigen::Vector4f centroid;
    //compute3DCentroid(*cloud, centroid);
    //ne.setViewPoint(centroid[0], centroid[1], centroid[2]);
	ne.setViewPoint(0, 0, 0);

    PointCloud<Normal>::Ptr cloud_normals(new PointCloud<Normal>());
    ne.compute(*cloud_normals);

    cout << "normal estimation complete" << endl;
    cout << "reverse normals' direction" << endl;

    //for (size_t i = 0; i < cloud_normals->size(); ++i) {
    //    cloud_normals->points[i].normal_x *= -1;
    //    cloud_normals->points[i].normal_y *= -1;
    //    cloud_normals->points[i].normal_z *= -1;
    //}

    cout << "combine points and normals" << endl;
    PointCloud<PointNormal>::Ptr cloud_smoothed_normals(new PointCloud<PointNormal>());
    concatenateFields(*cloud, *cloud_normals, *cloud_smoothed_normals);

    return cloud_smoothed_normals;
}


PolygonMesh::Ptr PointCloudHelper::greedyTrianglesReconstruction(
	PointCloud<PointNormal>::Ptr cloud)
{
	cout << "begin greedy triangles reconstruction" << endl;

	// Create search tree*
	pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
	tree2->setInputCloud(cloud);

	// Initialize objects
	pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	PolygonMesh::Ptr mesh(new PolygonMesh());

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius(5.025);

	// Set typical values for the parameters
	gp3.setMu(2.5);
	gp3.setMaximumNearestNeighbors(1000);
	gp3.setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
	gp3.setMinimumAngle(M_PI / 18); // 10 degrees
	gp3.setMaximumAngle(2 * M_PI / 3); // 120 degrees
	gp3.setNormalConsistency(false);

	// Get result
	gp3.setInputCloud(cloud);
	gp3.setSearchMethod(tree2);
	gp3.reconstruct(*mesh);

	cout << "greedy triangles reconstruction complete" << endl;
	return mesh;
}

PolygonMesh::Ptr PointCloudHelper::poissonReconstruction(
	PointCloud<PointNormal>::Ptr cloud)
{
    cout << "begin poisson reconstruction" << endl;
    Poisson<PointNormal> poisson;
    poisson.setDepth(8);
	poisson.setPointWeight(4);
	poisson.setSamplesPerNode(1.5);
	poisson.setInputCloud(cloud);
    PolygonMesh::Ptr mesh(new PolygonMesh());
    poisson.reconstruct(*mesh);
    cout << "poisson reconstruction complete" << endl;
    return mesh;
}

void PointCloudHelper::saveAsPLY(PolygonMesh::Ptr mesh, const string& fileName)
{
    cout << "saving mesh as PLY to " << fileName << endl;

	boost::filesystem::path file(fileName.c_str());
	if (boost::filesystem::create_directories(file.parent_path())) {
		cout << "Created directory "  << file.parent_path() << endl;
	}

    io::savePLYFile(fileName.c_str(), *mesh);
    cout << "mesh saved to " << fileName << endl;
}

void PointCloudHelper::saveAsVTK(PolygonMesh::Ptr mesh, const string& fileName)
{
	cout << "saving mesh as VTK to " << fileName << endl;

	boost::filesystem::path file(fileName.c_str());
	if (boost::filesystem::create_directories(file.parent_path())) {
		cout << "Created directory " << file.parent_path() << endl;
	}

	io::saveVTKFile(fileName.c_str(), *mesh);
	cout << "mesh saved to " << fileName << endl;
}

void PointCloudHelper::saveAsPCD(PointCloud<PointXYZ>::Ptr cloud, const string& fileName) {

	boost::filesystem::path file(fileName.c_str());
	if (boost::filesystem::create_directories(file.parent_path())) {
		std::cout << "Created directory " << "\n";
	}

	io::savePCDFileASCII(fileName.c_str(), *cloud);
	std::cerr << "Saved " << cloud->points.size() << " data points to " << fileName << std::endl;
}
