#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "DepthCapture.h"
#include "PointCloudHelper.h"

using namespace pcl;

int main(void) {
	cout << "Starting Structure Core Scanner\n";

	string folder = "foot-20190111";
	string file = "right";

	string input = "data/" + folder + "/" + file + ".occ";
	string output = "data/generated/" + folder + "/";


	cout << "Capturing scan to file (from OCC recording)\n";
	DepthCapture* depthCapture = new DepthCapture();
	depthCapture->captureAsPCD(input, output + file + ".pcd");
	delete depthCapture;

	cout << "Processing point cloud\n";

	PointCloudHelper* pointCloudHelper = new PointCloudHelper();
	PointCloud<PointXYZ>::Ptr cloud;
	cloud = pointCloudHelper->readPointCloudFile(output + file + ".pcd");
	
	cloud = pointCloudHelper->filterPointCloud(cloud);
	pointCloudHelper->saveAsPCD(cloud, output + file + "-filtered.pcd");

	cloud = pointCloudHelper->removeOutliers(cloud);
	pointCloudHelper->saveAsPCD(cloud, output + file + "-inliers.pcd");

	cloud = pointCloudHelper->smoothPointCloud(cloud);
	pointCloudHelper->saveAsPCD(cloud, output + file + "-smoothed.pcd");

	PointCloud<PointNormal>::Ptr cloudWithNormals = pointCloudHelper->estimateNormals(cloud);

	//PolygonMesh::Ptr mesh = pointCloudHelper->poissonReconstruction(cloudWithNormals);
	PolygonMesh::Ptr mesh = pointCloudHelper->greedyTrianglesReconstruction(cloudWithNormals);

	pointCloudHelper->saveAsPLY(mesh, output + file + ".ply");
	pointCloudHelper->saveAsVTK(mesh, output + file + ".vtk");

    delete pointCloudHelper;

	cout << "Finished Structure Core Scanner\n";
	return 0;
}