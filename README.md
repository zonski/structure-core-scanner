# Structure Core Scanner

## Setting up 

* Download and install Visual Studio 2015
* Download and install CMake
* Download and install PCL
* Clone the repo
* Go to the repo dir and run `cmake-gui .`
* Leave defaults (Source and Binaries directory should both be the repo base directory)
* Click "Configure", (choose "Visual Studio 2015 Win64" as the generator) 
* Click "Generate" (sometimes need to click this twice for some reason)
* This will have generated `structure_core_scanner.sln` in your project directory
* Open this file in Visual Studio (or click "Open Project" in the cmake GUI)
* Change the Startup Project to `structure_core_scanner` (right click on it in the Solution Explorer in the right)
* PCL seems to hang in Debug mode so switch to Release (forums suggest this, so I guess it's the only option)
* Download the data files (OCC) from [http://digitalpurpose.com.au/demo/corescanner/data]() and place this data 
directory inside your root project directory  
* Run the program - if things work, it should create files in data/generated
* Look at `main.cpp` to see the steps the program goes through, tweak code locally - ultimate goal is to create high 
quality 3D models (meshes) from the OCC (depth and RGB) data  



